import { Component } from '@angular/core';
import { NgForm } from '@angular/forms';
import { AngularFireDatabase, AngularFireDatabaseModule } from 'angularfire2/database';
import { AuthService } from './auth.service';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent {
  title = 'CAMT-Digital Marketing';

  constructor(private db: AngularFireDatabase, public auth: AuthService) {}

  logout() {
    this.auth.logout();
  }
}

