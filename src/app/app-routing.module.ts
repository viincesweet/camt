import { ModuleWithProviders } from '@angular/core';
import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { UserComponent } from './user/user.component';
import { HomeComponent } from './home/home.component';
import { RegisterComponent } from './register/register.component';
import { MyLearningComponent } from './my-learning/my-learning.component';
import { LoginComponent } from './login/login.component';
import { CourseComponent } from './course/course.component';
import { LearningcourseComponent } from './learningcourse/learningcourse.component';
import { QuestionTestComponent } from './question-test/question-test.component';
import { CourseMaterialsComponent } from './course-materials/course-materials.component';
import { LessonDetailComponent } from './lesson-detail/lesson-detail.component';
import { TestManagementComponent } from './test-management/test-management.component';
import { ForgotpasswordComponent } from './forgotpassword/forgotpassword.component';
import { UsermanageComponent } from './usermanage/usermanage.component';
import { UsertypeComponent } from './usertype/usertype.component';

const routes: Routes = [
  { path: '', redirectTo: 'login', pathMatch: 'full' },
  {path: 'user', component: UserComponent },
  {path: 'home', component: HomeComponent},
  {path: 'register', component: RegisterComponent},
  {path: 'my-learning', component: MyLearningComponent},
  {path: 'login', component: LoginComponent},
  {path: 'course', component: CourseComponent},
  {path: 'learningcourse', component: LearningcourseComponent},
  {path: 'question-test', component: QuestionTestComponent},
  {path: 'download-materials', component: CourseMaterialsComponent},
  {path: 'course-detail', component: LessonDetailComponent},
  {path: 'question-management', component: TestManagementComponent},
  {path: 'forgotpassword', component: ForgotpasswordComponent},
  {path: 'usermanage', component: UsermanageComponent},
  {path: 'usertype', component: UsertypeComponent},
  {path: '**', redirectTo: '/', pathMatch: 'full'}
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
