import { Component, OnInit, Injectable } from '@angular/core';
import { AngularFireDatabase } from 'angularfire2/database';
import { RouterModule, Routes } from '@angular/router';
import { NgForm } from '@angular/forms';
import { AuthService } from '../auth.service';
// import { moveIn, fallIn } from '../router.animations';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss'],
})
export class LoginComponent implements OnInit {
    // state: string = '';
    error: any;
    email: string;
    password: string;

  constructor(public db: AngularFireDatabase, public auth: AuthService) {}

  ngOnInit(): void {
  }

  login(data: NgForm) {
    alert(data.value.email + ' ' + data.value.password);
    this.auth.login(data.value.email, data.value.password);
    data.value.email = data.value.password = '';
  }
}
