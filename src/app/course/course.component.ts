import { Component, OnInit } from '@angular/core';
import { NgForm } from '@angular/forms';
import {AngularFireDatabase} from 'angularfire2/database';
import { Observable } from 'rxjs';

@Component({
  selector: 'app-course',
  templateUrl: './course.component.html',
  styleUrls: ['./course.component.scss']
})
export class CourseComponent implements OnInit {

  fileList: any;
  headers: any;
  options: any;

  constructor(private db: AngularFireDatabase) { }

  ngOnInit(): void {
  }

  fileChange(event) {
    this.fileList  = event.target.files;
    if (this.fileList.length > 0) {
        const file: File = this.fileList[0];
        const formData: FormData = new FormData();
        formData.append('uploadFile', file, file.name);
        this.headers = new Headers();
        /** In Angular 5, including the header Content-Type can invalidate your request */
        this.headers.append('Content-Type', 'multipart/form-data');
        this.headers.append('Accept', 'application/json');
        // this.options = new RequestOptions({ headers: this.headers });
    }
  }

  onFileChanged(event) {
    const courseImage = event.target.files[0];
  }
}
