import { Component, OnInit, Injectable } from '@angular/core';
import { AngularFireDatabase, AngularFireList, AngularFireAction } from 'angularfire2/database';
import { AngularFireAuthModule, AngularFireAuth } from 'angularfire2/auth';
import { RouterModule, Routes, Router } from '@angular/router';
import { NgForm } from '@angular/forms';
import { from, Observable, BehaviorSubject } from 'rxjs';
import { AuthService } from '../auth.service';
import { switchMap } from 'rxjs/operators';
// import { moveIn, fallIn } from '/router.animations';

@Component({
  selector: 'app-register',
  templateUrl: './register.component.html',
  styleUrls: ['./register.component.scss']
})

export class RegisterComponent implements OnInit {

  // state: string = '';
  error: any;
  email: string;
  password: string;
  memberList: AngularFireList<any>;
  member: any;
  members: any;

  constructor(public auth: AuthService, private router: Router, private db: AngularFireDatabase) {
    this.memberList = db.list('/member');
    this.members = this.memberList.valueChanges();
  }

  onSubmit(data: NgForm) {
    this.auth.signup(data.value.email, JSON.stringify(data.value.password));
    data.value.email = data.value.password = '';
    // Condition does not effective
    if (this.memberList.query.ref.key.indexOf('0') === -1) {
      this.db.list('/member').push(data.value);
    } else {
      // Foreach list to find duplicate value
      alert(this.memberList.query.ref.key.indexOf('0'));
    }
    data.resetForm();
  }

  ngOnInit(): void {
  }

}
