import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-learningcourse',
  templateUrl: './learningcourse.component.html',
  styleUrls: ['./learningcourse.component.scss']
})
export class LearningcourseComponent implements OnInit {

  coll = document.getElementsByClassName('collapsible');
  content;

  constructor() { }

  ngOnInit(): void {
    this.collapsePanel();
  }

  collapsePanel() {
    let i;
    for (i = 0; i < this.coll.length; i++) {
      this.coll[i].addEventListener('click', function() {
        this.classList.toggle('active');
        this.content = this.nextElementSibling;
        if (this.content.style.maxHeight) {
          this.content.style.maxHeight = null;
        } else {
          this.content.style.maxHeight = this.content.scrollHeight + 'px';
        }
      });
    }
  }

}
