import { Component, OnInit, NgModule } from '@angular/core';
import { NgForm } from '@angular/forms';
import {AngularFireDatabase} from 'angularfire2/database';
import { RouterModule, Routes } from '@angular/router';
import { BrowserModule } from '@angular/platform-browser';

@Component({
  selector: 'app-user',
  templateUrl: './user.component.html',
  styleUrls: ['./user.component.scss']
})

export class UserComponent implements OnInit {

  constructor(private db: AngularFireDatabase) {  }

  ngOnInit(): void {
  }

  editUser(data: NgForm) {
    this.db.list('/user').push(data.value);
    data.resetForm();
  }
}
