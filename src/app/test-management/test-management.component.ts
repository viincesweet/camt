import { Component, OnInit } from '@angular/core';
import { NgForm } from '@angular/forms';
import { AngularFireDatabase } from 'angularfire2/database';

@Component({
  selector: 'app-test-management',
  templateUrl: './test-management.component.html',
  styleUrls: ['./test-management.component.scss']
})
export class TestManagementComponent implements OnInit {

  // tslint:disable-next-line:variable-name
  private _fieldArray: Array<any> = [4];
  private newAttribute: any = {};
  isCorrect = 'Y';
  answer: any;

  constructor(private db: AngularFireDatabase) { }

  ngOnInit(): void {
  }

  addFieldValue() {
      this.fieldArray.push(this.newAttribute);
      this.newAttribute = {};
  }

  deleteFieldValue(index) {
      this.fieldArray.splice(index, 1);
  }

  addQuestion(data: NgForm) {
    this.db.list('/question').push(data.value);
  }

  public get fieldArray(): Array<any> {
    return this._fieldArray;
  }
  public set fieldArray(value: Array<any>) {
    this._fieldArray = value;
  }
}
