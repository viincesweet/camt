import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';

import { FormsModule, NgForm } from '@angular/forms';
import { AngularFireModule} from 'angularfire2';
import { firebaseConfig } from './../environments/firebase.config';
import { AngularFireDatabase, AngularFireDatabaseModule } from 'angularfire2/database';
import { HomeComponent } from './home/home.component';
import { RouterModule, Routes } from '@angular/router';

import { from } from 'rxjs';
import { UserComponent } from './user/user.component';
import { CourseComponent } from './course/course.component';
import { MyLearningComponent } from './my-learning/my-learning.component';
import { RegisterComponent } from './register/register.component';
import { LoginComponent } from './login/login.component';
import { QuestionTestComponent } from './question-test/question-test.component';
import { LearningcourseComponent } from './learningcourse/learningcourse.component';
import { TestManagementComponent } from './test-management/test-management.component';
import { CourseMaterialsComponent } from './course-materials/course-materials.component';
import { LessonDetailComponent } from './lesson-detail/lesson-detail.component';
import { CertificationComponent } from './certification/certification.component';
import { ForgotpasswordComponent } from './forgotpassword/forgotpassword.component';
import { UsermanageComponent } from './usermanage/usermanage.component';
import { UsertypeComponent } from './usertype/usertype.component';
import { AngularFireAuthModule, AngularFireAuth } from 'angularfire2/auth';
import { AuthService } from './auth.service';

const routes: Routes = [
  {path: '', pathMatch: 'full', redirectTo: '/home' },
  {path: '', component: HomeComponent},
  {path: 'user', component: UserComponent },
  {path: 'home', component: HomeComponent},
  {path: 'course', component: CourseComponent},
  {path: 'register', component: RegisterComponent},
  {path: 'my-learning', component: MyLearningComponent},
  {path: 'question-test', component: QuestionTestComponent},
  {path: 'download-materials', component: CourseMaterialsComponent},
  {path: 'course-detail', component: LessonDetailComponent},
  {path: 'question-management', component: TestManagementComponent},
  {path: 'forgotpassword', component: ForgotpasswordComponent},
  {path: 'usermanage', component: UsermanageComponent},
  {path: 'usertype', component: UsertypeComponent},
  {path: '**', redirectTo: '/', pathMatch: 'full'},
];

@NgModule({
  declarations: [
    AppComponent,
    HomeComponent,
    UserComponent,
    CourseComponent,
    MyLearningComponent,
    RegisterComponent,
    LoginComponent,
    QuestionTestComponent,
    LearningcourseComponent,
    TestManagementComponent,
    CourseMaterialsComponent,
    LessonDetailComponent,
    CertificationComponent,
    ForgotpasswordComponent,
    UsermanageComponent,
    UsertypeComponent
  ],
  imports: [
    BrowserModule,
    FormsModule,
    AngularFireModule.initializeApp(firebaseConfig),
    RouterModule.forRoot(routes),
    AngularFireDatabaseModule,
    AppRoutingModule,
  ],
  exports: [RouterModule],
  providers: [
    AuthService,
    AngularFireAuthModule,
    AngularFireAuth
  ],
  bootstrap: [AppComponent]
})

export class AppModule { }
