import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { LearningcourseComponent } from './learningcourse.component';

describe('LearningcourseComponent', () => {
  let component: LearningcourseComponent;
  let fixture: ComponentFixture<LearningcourseComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ LearningcourseComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(LearningcourseComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
