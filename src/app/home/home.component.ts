import { Component, OnInit } from '@angular/core';

import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';

import { AngularFireDatabase, AngularFireList } from 'angularfire2/database';


@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.scss']
})
export class HomeComponent implements OnInit {

  UserList: AngularFireList<any>;

  user: any[];
  slideIndex;
  slides = document.getElementsByClassName('mySlides');
  slideShowDots = document.getElementsByClassName('dot');

  constructor(db: AngularFireDatabase) {
    this.UserList = db.list('user');
  }

  ngOnInit(): void {
    // initial slide show
    this.showSlides(this.slideIndex);

  }

  // Start script slide show
  showSlides(n) {
    // Check if initial of slideshow index
    if (this.slideIndex === undefined) {
      this.slideIndex = 1;
    }
    let i;

    if (n > this.slides.length) {
      this.slideIndex = 1;
    }
    if (n < 1) {
      this.slideIndex = this.slides.length;
    }
    for (i = 0; i < this.slides.length; i++) {
      (this.slides[i] as HTMLElement).style.display = 'none';
    }
    for (i = 0; i < this.slideShowDots.length; i++) {
        this.slideShowDots[i].className = this.slideShowDots[i].className.replace(' active', '');
    }
    if (this.slides != null) {
      (this.slides[this.slideIndex - 1] as HTMLElement).style.display = 'block';
      this.slideShowDots[this.slideIndex - 1].className += ' active';
    }
  }
  // End Slide show

  // Onclick action previous & Next slide show
  plusSlides(n) {
    this.showSlides(this.slideIndex += n);
  }

  // Current Slideshow by Dots
  currentSlide(n) {
    this.showSlides(this.slideIndex = n);
  }
}
