import { CanActivate, Router } from '@angular/router';
import { AngularFireAuthModule, AngularFireAuth } from 'angularfire2/auth';
import { Injectable } from '@angular/core';
import { Observable, from } from 'rxjs';
// import { from } from 'rxjs';
import { AngularFireAction } from 'angularfire2/database';
// import { ninvoke } from 'q';

@Injectable()
export class AuthService {
    user: Observable<firebase.User>;
    constructor(private auth: AngularFireAuth) {
        this.user = auth.authState;
    }

    signup(email: string, password: string) {
        this.auth
          .auth
          .createUserWithEmailAndPassword(email, password)
          .then(value => {
            console.log('Success!', value);
          })
          .catch(err => {
            console.log('Something went wrong:', err.message);
          });
      }

      login(email: string, password: string) {
        this.auth
          .auth
          .signInWithEmailAndPassword(email, password)
          .then(value => {
            console.log('Nice, it worked!');
          })
          .catch(err => {
            console.log('Something went wrong:', err.message);
          });
      }

      logout() {
        this.auth
          .auth
          .signOut();
      }
}
