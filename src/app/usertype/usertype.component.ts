import { Component, OnInit } from '@angular/core';
import { NgForm } from '@angular/forms';
import { AngularFireDatabase, AngularFireList } from 'angularfire2/database';
import { from } from 'rxjs';
import { map } from 'rxjs/operators';

@Component({
  selector: 'app-usertype',
  templateUrl: './usertype.component.html',
  styleUrls: ['./usertype.component.scss']
})
export class UsertypeComponent implements OnInit {

  userTypeList: AngularFireList<any>;
  userTypes: any[];

  constructor(private db: AngularFireDatabase) {
    this.userTypeList = db.list('/usertype');
  }

  ngOnInit(): void {
    this.userTypeList.snapshotChanges().pipe(map(actions => {
      return actions.map(action => ({ key: action.key, value: action.payload.val() }));
      })).subscribe(items => {
        this.userTypes = items;
      });
  }

  addUserType(data: NgForm) {
    this.db.list('/usertype').push(data.value);
    data.resetForm();
  }

}

